//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

let adminUser = localStorage.getItem("isAdmin")

let courseId = params.get('courseId')

let enrolleeList;

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");

fetch(`http://localhost:3000/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {

	if (adminUser == "false" || !adminUser) {

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

	let enrollButton = document.querySelector("#enrollButton")

	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		fetch('http://localhost:3000/api/users/enroll', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//enrollment is successful
				alert("Thank you for enrolling! See you!")
				window.location.replace('./courses.html')
			}else{
				alert("Enrollment failed")
			}
		})
	})
	}else{
		fetch('http://localhost:3000/api/courses/detail', {
			method: "GET",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName
			})
		})
		let enrollButton = null
	}
})




//ORGINAL CODE


// let params = new URLSearchParams(window.location.search)

// let courseId = params.get('courseId')

// //retrieve the JWT stored in our local storage
// let token = localStorage.getItem("token");
// //retrieve the userId stored in our local storage
// let userId = localStorage.getItem("id");

// let courseName = document.querySelector("#courseName");
// let courseDesc = document.querySelector("#courseDesc");
// let coursePrice = document.querySelector("#coursePrice");
// let enrollContainer = document.querySelector("#enrollContainer");

// fetch(`http://localhost:3000/api/courses/${courseId}`)
// .then((res) => res.json())
// .then((data) => {
// 	courseName.innerHTML = data.name
// 	courseDesc.innerHTML = data.description
// 	coursePrice.innerHTML = data.price
// 	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

// 	let enrollButton = document.querySelector("#enrollButton")

// 	enrollButton.addEventListener("click", () => {
// 		//enroll the user for the course
// 		fetch('http://localhost:3000/api/users/enroll', {
// 			method: "PUT",
// 			headers: {
// 				'Content-Type': 'application/json',
// 				'Authorization': `Bearer ${token}`
// 			},
// 			body: JSON.stringify({
// 				courseId: courseId,
// 				userId: userId
// 			})
// 		})
// 		.then(res => {
// 			return res.json()
// 		})
// 		.then(data => {
// 			if(data === true){
// 				//enrollment is successful
// 				alert("Thank you for enrolling! See you!")
// 				window.location.replace('./courses.html')
// 			}else{
// 				alert("Enrollment failed")
// 			}
// 		})
// 	})
// })