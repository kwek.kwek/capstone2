const express = require('express')
const router = express.Router()
const auth = require("../auth")
const CourseController = require('../controllers/course')

//get all courses
router.get('/', (req, res) => {
	CourseController.getAllActive().then(resultsFromFind => res.send(resultsFromFind))
})

//get a single course
router.get('/:courseId', (req, res) => {
	let courseId = req.params.courseId
	CourseController.get({courseId}).then(resultFromFindById => res.send(resultFromFindById))
})

//create a new course
router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body).then(resultFromAdd => res.send(resultFromAdd))
})

//update a course
router.put('/', auth.verify, (req, res) => {
	CourseController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})

//archive (delete) a course
router.delete('/:courseId', auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.archive({courseId}).then(resultFromArchive => res.send(resultFromArchive))
})

module.exports = router